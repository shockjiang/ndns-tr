% !TEX root = main.tex
\section{Deployment \& Use Cases}\label{sec:deploy}
NDNS is implemented on both Mac OS X and Ubuntu platforms.
The source code locates at GitHub:
\url{https://github.com/named-data/ndns}.
And Ubuntu users could install NDNS directly by PPA repository
%, following the tutorial here:
%\href{http://named-data.net/doc/NFD/0.2.0/INSTALL.html#install-nfd-using-the-ndn-ppa-repository-on-ubuntu-linux}{http://named-data.net/doc/NFD/0.2.0/INSTALL.html}.
The implementation relies on ndn-cxx (C++ library with eXperimental eXtensions)\cite{prj:ndn-cxx} and NFD (NDN Forwarding Daemon)\cite{prj:nfd}.

The implementation contains a serial of tools, including name server daemon (ndns-daemon) which handles NDNS Query and Update, dig tool (ndns-dig) which performs iterative query to fetch RR associated with specific domain name,
ndns updater (ndns-update) which sends Update message to change RR, and some other management tools, such as ndns-add-rr, ndns-add-zone, etc., which help administrator to manage zone.


NDNS is deployed on NDN Testbed\cite{prj:ndntestbed}, and uses the same root key with the Testbed, which can be fetched from here:
\url{http://named-data.net/ndnsec/ndn-testbed-root.ndncert.base64}.
Serval zones are created according to the participating institutions of NDN Testbed.
For current, the NDNS instance hosted on NDN Testbed only contain NS RRs and ID-CERT RRs,
but it does allows authorized identity to store any content with any type.
What is more, certificate issued by administrator of NDN Testbed is stored in the NDNS instance.

The easiest way to gain benefits from NDNS is to use NDNS instance hosted by NDN Testbed,
since it the largest NDN network, and provides data transaction service for the end clients who connect to it.

Any user could apply for a namespace from NDN Testbed to publish content via NDN Testbed.
Administrator of NDN Testbed will verify the applicant manually,
and issue a certificate if it is approved.
The certificate is automatically stored in the NDNS instance.
The immediate use cases of the assigned namespace and issued certificate include:
1) generate packet with assigned namespace and sign it with the issued certificate,
   the packet can be authenticated through NDN Testbed;
2) the generated packet could be stored under the assigned namespace in the NDNS instance, this data is accessible to any end client.
3) the generated packet could also hosted locally, and provides accessibility through NDN Testbed.

\subsection{Use Cases}
\subsubsection{Content Verification}
As stated in previous sections,
any Data packet signed by the key whose certificate is stored in NDNS could be authenticated by the public.
Thus any two identity could authenticate each other's messages without requiring the two identity already have some pre-knowledge about the correspondent.

DNS-based Authentication of Named Entities (DANE)\cite{barnes2011use,hoffman2012dns} which make use of DNSSEC infrastructure to store certificates used by applications,
provides similar service as NDNS does here.
The most popular use case for DANE is transport Layer Security (TLS)\cite{dierks2008transport,hoffman2012dns},
which fetches certificates via DNSSEC to authenticate the TLS server, and exchange a  symmetric session key to encrypt content, which also is used to authenticate both parties.
DNAE TLS allows client/server applications to communicate in a way that is designed to prevent eavesdropping, tampering, or message forgery.

Compared to DANE TLS,
content verification through NDNS, requires that all producers store their certificate in NDNS,
and content is authenticated asynchronously without exchanging session key, which breaks the restriction of client/server applications.

As far as we know, there are at least two applications,
i.e., ChronoChat\cite{prj:chronochat}, which is a group chatting tool over NDN,
and NDN Video Player\cite{prj:ndnvideoplayer} which provides two tools, the server side which hosts video file and answers data requests, and the client side which consumes and play video,
use certificates in NDNS to authenticate content.

\subsubsection{Routing Scalability}
NDN leverages application name to routing directly,
routing scalability is a big concern among NDN research\cite{narayanan2011ndn,oran2015information,baid2012comparing},
some research even proposes that the routing entries is probably up to $O(10^{12} \sim 10^{15})$ compared to $O(10^9)$ in IP\cite{baid2012comparing,narayanan2011ndn}.

LINK\cite{maen2015} is recently proposed to address this concern.
LINK establishes an association between two name prefix,
which can bind a prefix to another globally routed prefix.
Together with map-and-encap process, which is transparent from the application perspectives:
the consumer applications send out ordinary NDN Interests for the data,
and the producer applications produce data in the namespace they own.
The first router that cannot forward the ordinary Interest,
would discover the corresponding the LINK object, and prepend it to the Interest.

In this way, Interest can be forwarded while the default routing zone (DFZ) only has to keep a manageable set of prefixes.
Furthermore, LINK and map-and-encap solution keeps all the benefits of NDN architecture.

During the whole process, there must be a network infrastructure component which stores all the LINK objects,
allows a stylized lookup mechanism to retrieve the LINK object (Data packet),
and is able to handle requests at Internet-scale.
NDNS is designed to be the infrastructure, due to the universality, scalability and origin authentication goals.

\subsubsection{Mobility Support}
Survey of mobility support in the Internet\cite{zhu2011rfc} summarize all the mobility support solution as essentially involving three basic components:
\begin{itemize}
\item a stable identifier for a mobile;
\item a Locator, which represents the mobile's current location, usually an IP address in IP network;
\item a mapping between the stable identifier and locator.
\end{itemize}

NDN is fundamentally different from IP.
NDN names content directly, uses name-based routing and adopts pull-based communication model.
In NDN, content consumer must send Interest in order to fetch Data,
and Data follows the reverse path of Interest to reach consumer, instead of forwarding according to routing table.
NDN benefits mobility in various aspects\cite{zhu2013new}:
\begin{itemize}
\item location independent data security;
\item enhance delivery with network cache;
\item integrate delay-tolerant network (DTN) and mobile ad-hoc network (MANET) into mobility support.
\end{itemize}

In NDN, consumer mobility can be supported by re-expressing the previous Interest.
And some mechanisms are proposed to address producer mobility support inspired by the new feature of NDN.
For example, Kite\cite{zhang2014kite} leverages the stateful forwarding plane to reach mobile nodes to make the location of a mobile node transparent to other parties in communication with it.
But still, Kite requires ``anchor'' to find the path to mobile in the first place for general scenario.

NDNS can kick in the producer mobility support,
by dynamically updating LINK object based on the latest network that the mobile residents.

Compared to the mobility support in IP, our solution follows the same idea,
but the stable identifier here is used to identify content,
instead of host, since content is the first-class citizen in NDN world.

\subsubsection{Service As a Storage}
The preliminary goal of NDNS is network storage,
the proposals that store content in DNS can migrate to NDNS easily,
since NDNS has no restriction on the format or content of stored data,
e.g., LOC RR\cite{davis1996means} for geographical location information about hosts, networks and subnets,
SSHFP RR\cite{schlyter2006using} for publish secure shell (SSH) key fingerprint,
DNSBL RR and DNSWL RR\cite{levine2010dns} for blacklist and whitelist,
SRV RR\cite{gulbrandsen2000dns} for server information of specific service.
And the RRs' naming is intuitional, following the naming convention defined in previous section,
e.g., the RR for location of server can be /ucla/cs/host1 hosted in zone /ucla/cs is named /ucla/cs/NDNS/host1/LOC/%FD%00.

Here we build an application using NDNS as a storage.
The application, called CPUSensor\cite{prj:cpusensor}, helps a PC administrator monitor the PC's CPU temperature and backup data to NDNS.
The naming for the Data is \\
 \[<ZoneName>/NDNS/<IdentityNameWithoutZonePart>/CPU-INFO/<VersionNumber>\]

For example, the identity /edu/ucla/shock stores Data named /edu/ucla/NDNS/shock/CPU-INFO/\%FD\%23 in NDNS.
The data is sent to zone by Update message, and everyone could fetch it from NDNS.
The data stored in NDNS is accessible by the public,
and this temperature information could be encrypted if the administrator want to keep the information confidential.
