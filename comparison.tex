%!TEX root = main.tex
\section{Comparison Between NDNS and DNS}
The preliminary goal of DNS is a consistent namespace which will be used for referring to content resources.
It leverages tree-like hierarchical domain namespace,
with resources associated with nodes and leaves in the tree.
Resources are organized into zones based on domain name they are associated with.
Name servers are structured following the zone hierarchy to host the resources.
Caching resolvers are created to aggregate identical requests, cache and reuse passing-by resources.
Each DNS zone is required to have multiple zone servers, one is the master, and the rest are slaves.
Any modification to the zone data is done at the master server, and later propagate to slaves through zone transfer.

%Name servers and caching resolver are usually merged into one host.

Two kinds of queries are designed to discover resource, iterative and recursive queries.
Recursive query delegates the resource discovery task to another host.
Iterative query discovers the data with very limited prior knowledge,
e.g., the IP address of at least one root server.
This mechanism uses intermediate result, say referral to servers of next zone,
as input of next query until target resource is fetched, or proved to be non-existing.
Normally, a end client sends recursive query to its local caching resolver,
which then performs a serial of iterative queries to find the requested resource,
and returns it to the end client.

DNS is proved to be great success, according to our understanding, the following design is of significance reason for its wide deployment and use.
\begin{itemize}
\item Universality:
  DNS is not designed for specific application(s), it can store data from any application.
\item Scalability:
  Hierarchical structure scatters load to different servers,
  and cache remits the load pressure to root zone and its children zones.
  This design scales the lookup and DNS is able to provide Internet-scale service.
\item Autonomy and Robustness:
  DNS is maintained in a distributed way, and administrator of a zone not only full control over its data content,
  but also full control over the service provisioning, including how many replicants and where to put them.
  Furthermore, each zone's decision only affects availability of their own and possibly their descendants, not others.
\item Immediate need for DNS: The popular email and web need a system to resolver their application names, which leads to big force to deploy DNS.
\end{itemize}

After the initial proposal, some significant enhancements is proposed by following works to address new requirements over the Internet's evolutions,
including DNSSEC or more precisely, DNSSEC-bis\cite{arends2005resource,arends2005protocol,arends2005dns}, to address content authentication,
DynDNS\cite{eastlake1997secure,wellington2000secure} to address dynamic content update,
AXFR\cite{lewis2010dns} and IXFR\cite{ohta1996incremental} to transfer zone from master server to slaves,
EDNS0\cite{vixie1999extension} and EDNS(0)\cite{damas2013extension} to advertise the allowable maximal DNS message size over UDP, etc.

%Most of the enhancements are added with by defining new types of resource record,
%new flag bits and new process to handle requests.

NDNS inherits the above successful design of DNS together with significant enhancements.
But due to the fundamental differences between their underlying protocol,
i.e., IP names the host while NDN names content directly,
there are significant differences when it comes to the detailed mechanism:
\begin{itemize}
\item DNS implements caching on application layer, because network and transport layer identifier cannot identify content permanently.
  DNS Servers could fully use application-layer information to find the best-match (target RR or referral to zone nearest to the target) to answer queries when all DNS query contains the target domain name and resource type.
  Thus DNS iterative query follows zone hierarchy of the domain name and possibly skip intermediate zones.
  NDNS leverages network layer cache.
  NDN routers could only do name-based match, unnecessary information reduce the possibility to reuse cached data.
  Thus, We propose a new iterative query which contains only necessary information and follows name hierarchy of target domain name strictly.
\item Security serves as an enhancement for DNS, however,
  it is built-in feature for NDNS.
 Resource in NDNS self-contains its authentication information.
\item DNS replies on master name server to handle update,
while in NDNS all the servers in a zone are equal, and every individual of them is able to handle update, to better satisfy the needs of prosperous of mobile devices and emerging requirements of mobility support.
  Consequently, NDNS must adopt multiple-master model to synchronize zone,
  comparing to the master-slave model of DNS zone transfer.
\item DNS has strict size limitation when DNS message is transmitted over UDP,
  while TCP eliminates size limitation but with extra three-way handshake and a pair of two-way handshakes as cost for every single iterative query.
  NDNS segments large content resource into multiple NDN Data packet, and let NDN transmit individual packet.
\item DNS stores only one version of resource associated with specific domain name; while NDNS allows multiple versions.
  This new feature satisfies the requirements of the immutable data model adopted by NDN, and allows NDNS work in more scenarios.
\end{itemize}

In the rest of this section, we first quote definitions of some basic concepts used by DNS and NDNS from RFC 1034,
including domain namespace, resource record (RR), name server and zone.
Based on those concepts,
we then present the detailed similarities and differences between NDNS and DNS listed above.

\begin{quote}
``The DOMAIN NAMESPACE and RESOURCE RECORDS,
which are specifications for a tree structured namespace and data associated with the names.
Conceptually, each node and leaf of the domain namespace tree names a set of information,
and query operations are attempts to extract specific types of information from a particular set.''
\attrib{\emph{RFC 1034, Page 6}}
\end{quote}

Information in the set are classified by ``type'', e.g., ``NS'' for referral to name server,
``ID-CERT'' for identity certificate data, ``TXT'' for generic data encoded in ascii text format.
The namespace is tree-like hierarchical with the hierarchy,
and names using ``/'' in NDNS, and ``.'' in DNS as the character to mark the boundary between domain name hierarchy levels.

\begin{quote}
``NAME SERVERS are server programs which hold information about the domain tree’s structure and set information.
In general a particular name server has complete information about a subset of the domain space,
and pointers to other name servers that can be used to lead to information from any part of the domain tree.
Name servers know the parts of the domain tree for which they have complete information;
a name server is said to be an AUTHORITY for these parts of the namespace.
Authoritative information is organized into units called ZONEs,
and these zones can be automatically distributed to the name servers which provide redundant service for the data in a zone.''
\attrib{\emph{RFC 1034, Page 6}}
\end{quote}

Zone names define the administrative boundaries in the name-tree.
The zone that contains RR associated with root of the name-tree is called root zone, represented by ``/'' in NDNS and ``.'' in DNS.
For a specific zone, the directly connected zone in the name-tree that nearer to the root zone is its \emph{parent zone},
and the directly connected zone that further to the root zone is its \emph{child zone}.
%The zone name also constructs zone hierarchy for domain name.

\subsection{Caching \& Query Comparsion}
DNS is built on IP, which names host.
Since IP addresses (source and destination addresses) cannot identify a content, i.e., a specific DNS message,
while transport layer identifier (ports, address, sequence number) can only identify content during a session,
as a consequence, caching in DNS is implemented on application layer.
That's why DNS relies on servers to process cache and reuse data.
To make fully use of application layer caching,
DNS query contains as much information as possible, i.e., the target domain name and resource type,
and servers is intelligent enough to find the best-matched DNS Response to answer the query.
Here best match denotes to the target RR or referral to the zone nearest to destination zone in servers' local database.
In this case, a serial of iterative queries are sent to destination zones following the \emph{zone hierarchy} of target domain name,
together with possible \emph{zone jump}, which means to skip some intermediate zones.

For example, to fetch TXT RR associated with domain name www.ndnsim.net.,
the end should send queries containing the target domain name and resource type to root zone, zone net., zone ndnsim.net. sequentially.
Those queries follow zone hierarchy of the target domain name, but some query could be reduced due to best-match may returns cached response which is nearer to the target content than the referral of next zone.
Say root zone may answer the query with
1) referral to zone net., then next query is sent to zone net., following the zone hierarchy;
or 2) referral to zone ndnsim.net., then next query is sent to zone ndnsim.net., skipping zone net.;
or 3) the target content, TXT RR associated with name www.ndnsim.net., skipping both intermediate zones.
And the priority ranking of the three responses is $3) > 2) > 1)$.

NDNS is built for NDN, which names content directly.
NDNS leverages network layer cache hosted by NDN routers since name of Data packet could identity content.
But NDN routers is unable to find best match according to application-level information,
but only make use of name-based match.
It is possible for NDNS to implement the similar zone hierarchy iterative mechanism.
For example, name servers of zone /net encapsulates their best-matched result, a referral to zone /net/ndnsim, named /net/NDNS/ndnsim/NS
\footnote{The naming convention will be explained later},
in a Data packet named /net/NDNS/ndnsim/www/TXT to answer the query contain the ultimate question.
In this case, the cached Data /net/NDNS/ndnsim/www/TXT cannot be used to answer query for other domain name,
but logically, the embedded referral /net/NDNS/ndnsim/NS is able to answer any other domain query to zone /net for /net/ndnsim or its descendants.

The reason which disables the potential ability is that the query contains excessive information,
which cannot benefit name-based matching on routers, on the contrary, reduce the possibility to reuse cached data.
To fully utilize contents cached by NDN router,
We propose another iterative query mechanism which contains only necessary information and follows the \emph{hierarchy of target domain name}.
For example, to fetch TXT RR associated with name /net/ndnsim/www,
NDNS sends /NDNS/net/NS to root zone,
/net/NDNS/ndnsim/NS to zone /net,
and /net/ndnsim/NDNS/www/TXT to zone /net/ndnsim.
\footnote{More accurate process will be explained later}
Every single query contains only necessary information,
and the serial of queries strictly follow the name hierarchy of target domain name.
In this case, referral to a zone's name server could be reused to serve requests finally targeting to the zone and its descendants.
And iterative queries to root zone or its children are most likely to be satisfied by cached content in NDN network,
which eliminates the bottleneck and redirects the traffic to wide-spread name servers belonging to grandchildren of root zone.

%DNS replies on caching resolver to cache data,
%otherwise, DNS name servers could not survive under the heavy traffic from the whole Internet.
Since NDN network could aggregate identical queries and cache NDNS response,
which is the design goal of caching resolver,
caching resolver in NDNS plays a less important role than in traditional DNS.
But still caching resolver helps to offload computing and save energy,
especially for devices like sensor, wearable devices and personal digital assistants (PDAs).

%multicast and aggregation is much more easily achieved by network cache.

\subsection{Security \& Update \& Zone Synchronization Comparison}
DNS security is enhanced by DNSSEC.
Mainly four new RR types is added, i.e., RRSIG, DNSKEY, DS and NSEC.
DNSKEY contains public key material,
RRSIG contains the signature with a specific RR
and Key Tag to identity the DNSKEY RR containing the public key that a validator can use to verify the signature.
DS refers to a DNSKEY RR by storing the key tag, algorithm number, and a digest of the DNSKEY RR.
NSEC is used to answer the queries to non-exist records.

DNSSEC adopts hierarchical trust model,
and allows end consumer to build authentication chain via following schema:
\[TrustAnchor->[DS->DNSKEY]^*->TargetRR\]
Here ``*'' denotes zero or more $DS->DNSKEY$ subchains.

RRSIG appears in the chain explicitly,
because any DS, DNSKEY, and target RR in this chain must be bound with a RRSIG.
DNSSEC permits more complex authentication chains, such as additional layers of DNSKEY RRs signing other DNSKEY RRs within a zone.

NDNS follows the similar way to build the authentication chain from trust anchor to any target RR.
RR stored in NDNS is encapsulated in Data packet, which contains signature and certificate name of the key to generate the signature.
Keys are stored in NDNS in the form of certificates (also NDN Data packet), which includes a signature and certificate name of the key to generate the signature.
Compared to the mechanism of DNSSEC, our advantages include
1) every RR self-contains the authentication information, instead of extra resource records;
2) certificates are referred by its name directly, instead of hashing or digest.
This two content-centric style advantages simplifies the constructing of authentication, logically and engineeringly.

What is more, NDNS does not need NSEC, but requires online signing,
which has several security risks,
including an increased likelihood of private keys being and an increased risk of denial of service attack.

RR update heavily relies on security mechanism.
DynDNS defines to two update modes,
and both modes require that the key signing the update message must be stored in DNS.

For Mode A, any dynamically added or changed RRs are signed by their authorizing dynamic update key. Dynamic RRs are stored,
along with this SIG RR, in a separate online dynamic master file.
However, the AXFR SIG RR (for zone transfer) and NXT RR (indicating the nonexistence of a name in a zone, obsoleted by NSEC) which cover the zone under the zone key will not cover dynamically added data.
For Mode B, the RR are re-signed by the zone's key, together with recalculating AXFR SIG and NXT RRs.
%One reasonable operational scheme may be to keep a mostly static main zone operating in Mode A and have one or more dynamic subzones operating in Mode B.

NDNS update mechanism mainly follows Mode A but without side-effects of zone synchronization or non-existence of RR,
and also requires that keys' certificate must be stored in NDNS.
Authorized identity signs the RR and send the update message to the name server,
which would validate it and store it without re-signing.

A significant difference is that DNS Update must be sent to master server,
while in NDNS, all name servers in specific zone are peering with each other,
and any of them is capable to handle update message.
The rationality here is that original DNS assume that ``Most of the data in the system will change very slowly''.
and we believe this assumption is not proper due to the prosperous of mobile devices and emerging requirements for mobility support.
NDNS update approach improves:
1) robustness, even if the network is partitioned to different areas,
DNS could handle update in partitioned areas,
not mention the single point of failure of master name server;
2) scalability, any name server can handle update,
thus the update load could be distributed among all name servers instead of to one specific name server.

Distributed update approach leads distributed zone synchronization in NDNS.
DNS zone transfer mainly adopts master-slave model,
while NDNS adopts multiple-master replication model for zone synchronization.
Here we use \emph{zone synchronization} since all servers are equal.
There are two different approaches, full zone transfer (AXFR), incremental zone transfer (IXFR) for zone transfer in DNS.
Both approaches are based on master-slave model, although slave-to-slave synchronization is defined as optional action in original proposal.

\subsection{Segmenting \& Versioning}
DNS does not really restrict the size of DNS message if the DNS message is transmitted over TCP,
but the maximum allowable size of a DNS message over UDP not using the extensions mechanism is 512 bytes,
and truncated UDP response triggers retry over TCP which leads to three-way handshake to establish connection and a pair of two-way handshakes to terminate connection.
EDNS0 and EDNS(0)\footnote{EDNS0 is obsoleted by EDNS(0)} allow requestors to advertise their capabilities to responders, and thus avoid TCP when it is possible.

In NDNS, RR is encapsulated in NDN Data packet.
It is possible that RR outsizes the upper limit of single Data packet.
Thus, RR is allowed to be encapsulated in multiple Data packet with different segment numbers.
And there is no handshakes for NDNS message exchange, even for large RR.
Still, the size boundary of NDN Data packet is still on-going research, the maximum allowable size of a NDN message remained to be addressed which is beyond the scope of NDNS.

NDNS provides a new feature, versioning.
Versioning is an important feature of immutable data model adopted by NDN.
In NDN, it is a fact that there are possibly multiple versions of data,
thus, NDNS should be capable to store multiple version of RR associated with same name.
This could be requirements from some applications,
e.g., to store weather information of Los Angeles sampled every day.
The new feature extends the adhibition of NDNS.
%Versioning also benefits the transfer period for applications,

%versioning makes this case very simple, while mutable data model need.
%It is also beneficial for end consumer to distinguish and fetch data with specific version.