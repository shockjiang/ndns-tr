# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# @brife My main goal to adopt waf in this project is to learn waf
# @note Python code style adopted by Google: https://google-styleguide.googlecode.com/svn/trunk/pyguide.html

from waflib import Logs
from waflib import Utils
from waflib import Context

import os

APPNAME = "ndns-tr"
VERSION = "0.30"

top = "."
out = "build"

def options(ctx):
    ctx.add_option("--foo", action = "store", default=False, help="Just learn to use option")

def configure(ctx):
    ctx.load('tex')
    if not ctx.env.LATEX:
        ctx.fatal('The program LaTex is required')
    ctx.find_program("cp", var="COPY")  # ctx.env.COPY is set to be cp
    ctx.env.FOO = ctx.options.foo  # ctx.env.FOO is set

def build(ctx):
    ctx(features = "tex",
        name = "compile",
        type = "pdflatex", # must be pdflatex
        outs = "pdf",
        source = "main.tex",
        prompt = 0,
        )

    ctx(features = "subst",
        source = "main.pdf", # working dir is bldnode
        target = "../ndns-memo-v%s.pdf" % VERSION,
        use = "compile",
        name = "rename",
        is_copy = True,
        )
    ctx(
        source = "main.pdf",
        rule = "open ${SRC}",
        )
