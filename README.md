This is NDNS technical report
=============================

This report present NDNS (NDN-based Domain Name System) which solves the following operational chanenges of NDN network: undesirable namespace clashes or conflicts, cryptography support and scalable forwarding mechanism. NDNS achieves the goal by serving as an authoritative database that allows storing:
* Namespace delegation information to avoid namespace clashes or conflicts;
* Cryptographic information for content-verification;
* Mapping between routable and non-routable names. This \paper proposes a design for  NDNS  inspired by traditional DNS (Domain Name System) and DNSSEC (Domain Name System Security Extensions).

The ultimate goal of NDNS is to provide running, scalable, and universal data storage in NDN.
%In current stage, NDNS mainly maintains the hierarchical namespace,  and distributes public keys.
The only precondition for using NDNS is that the data is named hierarchically compliant with NDN:

* Running means deployed. Thus we require a functioning system, not just a simple theoretical model.
* Scalable means it can support internet-scale service. The distributed and hierarchical structure used in DNS has proven to sufficiently support internet-scale service.
* Universal means supporting a variety of data types that allows the functionality of NDNS to be extended beyond a simple public key distribution service. For example, forwarding hints can be stored in NDNS to solve routing scalability and mobility issues.


Now NDNS is looking for use case. If you want to make use of NDNS, do not hesitate to contact us: Xiaoke Jiang <shock.jiang@gmail.com>
