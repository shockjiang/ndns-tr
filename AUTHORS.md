Authors
===

*  Xiaoke Jiang                <http://netarchlab.tsinghua.edu.cn/~shock/>
*  Alexander Afanasyev   <http://lasr.cs.ucla.edu/afanasyev/index.html>
*  Jiewen Tan                   <alanwake@ucla.edu>
*  Lixia Zhang                  <http://cs.ucla.edu/~lixia/>

###Acknowledgements

A lot of guys in  NDN groups helped to review the report and gave comments/criticism/suggestions. They are:
* Michael Sweatt          <https://www.linkedin.com/in/michaelsweatt>
* Junxiao Shi                <http://www.cs.arizona.edu/people/shijunxiao/>
* Spyridon Mastorakis  <http://cs.ucla.edu/~mastorakis/>
