%!TEX root = main.tex
\section{Implementation} \label{sec:imp}
In this section, we present the detailed implementation of NDNS, including database schema and format of NDNS packets.
The current NDNS implementation includes the core part of the above design, including Query/Response, Update/Result, name server query tool, and a set of management tools.
However, Segmenting, versioning, zone synchronization and LINK object are not implemented in current release yet.

\subsection{Database}

In NDNS implementation, two tables are created in database, zones and rrsets.

The current implementation allows multiple zones share the same database,
so table zones is created to store zones' information.
Scheme of table zones is shown in Table \ref{tab:zones}.

\begin{table}[H]
\caption{Table Zones Scheme} \label{tab:zones}
\begin{center}
\begin{tabular}{lcll}
\toprule[1.5pt]
Column & Data Type & Meaning & Constraint\\
\midrule[1pt]
id & uint64 & identifier of each row & primary key\\
name & blob & zone's name & unique\\
ttl &unsigned int & Default TTL of the zone related rrset & cannot be null, 3600 is default value\\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}%

\begin{table}[H]
\caption{Table Rrsets Scheme} \label{tab:rrsets}
\begin{center}
\begin{tabular}{lcll}
\toprule[1.5pt]
Column & Data Type & Meaning & Constraint \\
\midrule[1pt]
id & uint64 & identifier of each row & primary key\\
zone\_id & uint64 & foreign key to Table Zones & cannot be null\\
label & blob & label of name & cannot be null\\
type & blob & type of rrset, for example, ID-CERT & unique(zone\_id, label, type)\\
version & blob & version of this record & can be null\\
ttl &unsigned int & TTL of this record & can be null, when the zone's ttl should be used\\
data & blob & the wired Data packet contains the record & can be null\\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}%

RRs are stored in rrsets table as shown in Table \ref{tab:rrsets}.
Name servers do not construct Data packets if the requested RR is in the database,
so Name servers store the wire format of the Data packet directly,
and respond to requests with the well-formed pre-constructed Data packet directly.
The pre-constructed Data could be reused once the corresponding queries come util its certificate expires.

\subsection{NDNS Packet Format}
In this section, we describe the format of NDNS packet, including: Query, Update and their Responses.

\subsubsection{Query Message}
Query packet is an Interest, which contains the NDNS Name. The format of Interest is described here \cite{ndnfmt:interest}, and the name convention is defined in Table \ref{tab:nameconv}. And the values of application tag is limited and shown in Table \ref{tab:svctag}.
\begin{table}[H]
\begin{center}
\caption{Application Tag}\label{tab:svctag}
\begin{tabular}{cll}
\toprule[1.5pt]
Application Tag & Value & Comment \\
\midrule[1pt]
NDNS & Iterative query, except ID-CERT query & name server announce prefix ending with this tag \\
KEY & query for ID-CERT record & name server announce prefix ending with this tag\\
NDNS-R & Recursive query & caching resolver announce prefix ending this this tag\\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}
Note again, the name of query does not contain version number. And the value of Type field in the name of query message cannot be ``UPDATE''.

\subsubsection{Iterative Query Response Message}\label{sec:queryresponse}

NDNS allows universal Data packet stored in the system,
which implies that it does not restrain the format of Response, the only assumption is that the Responses are NDN Data packet.
However, in order to transmit NDNS-level data to support NDNS mechanism, a specific format of Content is defined.

Besides, NDNS defines an optional application meta information, named NdnsType in MetaInfo.
Data packet is made up by multiple fields, including Name, MetaInfo, Content and Signature.
The format is shown in Table \ref{tab:resfmt}.

\begin{table}[H]
\begin{center}
\caption{NDNS Response Packet Format}\label{tab:resfmt}
\begin{tabular}{cl}
\toprule[1.5pt]
Response ::= & DATA-TLV TLV-LENGTH \\
& Name \\
& MetaInfo (=NdnsMetaInfo) \\
& Content\\
& Signature\\
Name & described in Section \ref{sec:name} \\
NdnsMetaInfo & described in Section \ref{sec:ndnsmetainfo} \\
Content & described in Section \ref{sec:content} \\
Signature & described in Section \ref{sec:signature}\\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}


\paragraph{NdnsMetaInfo} \label{sec:ndnsmetainfo}
NdnsMetaInfo is derived type of MetaInfo\cite{ndnfmt:metainfo}. Here we define the NdnsMetaInfo

\begin{table}[H]
\begin{center}
\caption{NdnsMetaInfo Format}\label{tab:metainfo}
\begin{tabular}{rl}
\toprule[1.5pt]
NdnsMetaInfo ::= & NDNS-METAINFO-TLV(=METAINFO-TLV) TLV-LENGTH \\
& ContentType(=0, Blob) \\
& FreshnessPeriod \\
& NdnsType?\\
NdnsType ::= & NDNS-TYPE-TLV(=180) TLV-LENGTH \\
& nonNegativeIntegear \\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}

%In the implementation, NdnsType is contained by MetaInfo as a AppMetaInfo in latest ndn-cxx.

For an NDNS packet, the ``ContentType`` should always be ``BLOB`` (=0).

FreshnessPeriod is describe in \cite{ndnfmt:freshnessperiod}.

NdnsType has some but limited values listed in Table \ref{tab:ndnstype}.
\begin{table}[H]
\begin{center}
\caption{NdnsType Values}\label{tab:ndnstype}
\begin{tabular}{rll}
\toprule[1.5pt]
Value & Meaning \\
NDNS-RESP = 1 & A positive NDNS Response. Positive means having the requested data\\
NDNS-NACK = 2 & A negative NDNS response. Negative means cannot find the requested data\\
NDNS-AUTH = 3 & Zone with longer name prefix exists\\
\midrule[1pt]
NDNS-RAW = 0 & A logic type indicating that the Response does not contain NdnsType\\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}

Response contains a NdnsType in its MetaInfo field to distinguish packet type, which may not contain in normal Data packet.
Those Response without containing NdnsType field explicitly is treated as positive response to NDNS query.
NdnsType is an optional field, RR could ignore it and use standard MetaInfo directly.
For example, the certificate store at the name server, which does not contain NdnsType field, also serves as final response to a iterative query.

\paragraph{Content}\label{sec:content}
It’s the end application which knows exactly the format of they content filed,
and NDNS does not have more constraints on the standard NDN content format described here \cite{ndnfmt:contenttype}.

But still, there are some results from NDNS are not constructed by end applications,
e.g., the intermediate results of iterative queries.
Thus, two content format is define in Table \ref{tab:contentfmt}.
One format is compatible with universal Data packet.
The other provides a standard format could be understand by all NDNS tools.
%The standard RR format could be resolved by name servers

\begin{table}[htdp]
\begin{center}
\caption{Content Format Used by NDNS Itself} \label{tab:contentfmt}
\begin{tabular}{rl}
\toprule[1.5pt]
Content ::= & CONTENT-TYPE TLV-LENGTH \\
& BYTE* $|$ STANDARD-RRDATA*  \\
%RR ::= & RR-TYPE-TLV(=190) TLV-LENGTH \\
%& RRDATA \\
STANDARD-RRDATA ::= & RRDATA-TYPE-TLV(=191) TLV-LEGNGTH \\
& BYTE*\\

\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}



\paragraph{Signature}\label{sec:signature}
Signature is described here \cite{ndnfmt:sig}.
In most case, RRs are signed by end publishers before it is stored in NDNS.
NS records are constructed by name servers.
NDNS-NACK response is constructed by authoritative name server if no such record is stored at the name server\footnote{end publishers could also stored NDNS-NACK in NDNS}.

\subsubsection{Recursive Query Response Message}
Recursive query response is normal Data packet, whose name is inherited from query Interest,
and fill the content field with the wired format of final response of iterative query.
Since embed response is already signed by authorized identity, it's not necessary to sign recursive query response. The format is shown in Table \ref{tab:recurresfmt}.
\begin{table}[htdp]
\begin{center}
\caption{Recursive Query Response} \label{tab:recurresfmt}
\begin{tabular}{rl}
\toprule[1.5pt]
Response ::= & DATA-TLV TLV-LENGTH \\
 & Name \\
 & MetaInfo \\
 & Content \\
 & Signature \\
\midrule[1pt]
MetaInfo & Described in \cite{ndnfmt:metainfo}\\
Content & The wire format of whole final response of Iterative query \\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}

An alternative response format for recursive query is to allow caching resolver to fill the content field with the content of content extracted from final response of iterative query,
and sign the packet with its own key.

The alternative way does not pass original RR to end consumer,
which may not works when the RR is encrypted,
or end consumers' trust policies has special requirement.


\subsubsection{Update Message}
Update message is a NDNS Request (Interest) which encapsulates a NDNS Response (Data) in its name.
The format of name of NDN Query is described in Section \ref{sec:name},
with putting wired format of the latest RR in the Label field,
and application tag ``UPDATE'' in Type field,
as defined in Table \ref{tab:updatename}.

\begin{table}[H]
\begin{center}
\caption{Update Name Convention}\label{tab:updatename}
\begin{tabular}{lll}
\toprule[1.5pt]
 & Name Component & Type of the Component \\
 \midrule[1pt]
Update Name ::= & Zone Name & NDN Name Type \cite{ndnfmt:name}  \\
			& Application Tag (=NDNS) & BYTE+\\
			& Embed Response & Wired Format Of Query Response, Section \ref{sec:queryresponse} \\
			& Type (=UPDATE)& BYTE+ \\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}


\subsubsection{Update Response Message}
Update response is normal Data packet, whose name is inherited from Update Interest, and its content field is filled with update result message.
\begin{table}[H]
\begin{center}
\caption{Update Response}\label{tab:updateresname}
\begin{tabular}{lll}
\toprule[1.5pt]
Update Response ::= & DATA-TLV TLV-LENGTH \\
                    & Name \\
                    & MetaInfo \\
                    & Content\\
                    & Signature \\
 \midrule[1pt]
Content ::= & CONTENT-TYPE-TLV TLV-LENGTH \\
            & Result (=STANDARD-RRDATA) \\
 \midrule[1pt]
Result ::= & RRDATA-TYPE-TLV (=191) TLV-LENGTH \\
           & Return-Code \\
           & Return-Msg? \\
\midrule[1pt]
Return-Code ::= & RETURN-CODE-TLV(=160) TLV-LENGTH \\
                & NonNegativeInteger \\
Return-Msg ::= & RETURN-MSG-TLV(=161) TLV-LENGTH \\
               & BYTE* \\
\bottomrule[1.5pt]
\end{tabular}
\end{center}
\end{table}

If update succeeds, the return code is 0; otherwise, non-zero value is returned.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: