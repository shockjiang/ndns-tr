# change this to the file name of the paper
PRJ = main
#BIB_FILE = /Users/shock/shock-rsp/2_PaperWriting/sigproc.bib
BIB_FILE = ndns.bib
DVIOPT = -P cmz -t letter -G0

K2PDF = k2pdfopt
K2PDF_OUT = ndns-kindle
K2PDF_PARA = -dev kp2 -ui- -x -o ${K2PDF_OUT}
SEND2KINDLE = /usr/local/bin/sendKindle

all : ${PRJ}.pdf tokindle

#${PRJ}.dvi : *.tex *.bib
#	latex ${PRJ}; bibtex ${PRJ}; latex ${PRJ}; latex ${PRJ}
${PRJ}.pdf : *.tex
	pdflatex ${PRJ} && bibtex ${PRJ}.aux;
	@if [ -a ${BIB_FILE} ] ; \
	then \
		bibtex ${PRJ}.aux ; \
	fi
	pdflatex ${PRJ}
	pdflatex ${PRJ}

tokindle: ${PRJ}.pdf
	@if which ${K2PDF} > /dev/null && which ${SEND2KINDLE} > /dev/null; \
	then \
		${K2PDF} ${PRJ}.pdf ${K2PDF_PARA}; \
		${SEND2KINDLE} ${K2PDF_OUT}.pdf; \
	else \
		echo "no k2pdfopt or sendKinle found. will not send file to kindle"; \
	fi

#

#${PRJ}.ps : ${PRJ}.dvi
#	dvips ${DVIOPT} -o ${PRJ}.ps ${PRJ}.dvi
#
#${PRJ}.pdf : ${PRJ}.ps
#	ps2pdf ${PRJ}.ps ${PRJ}.pdf
#
#dvi : ${PRJ}.dvi
#
#ps : ${PRJ}.ps
#
#pdf : ${PRJ}.pdf

#view : ${PRJ}.dvi
#	xdvi -s 0 ${PRJ}.dvi &

#viewps : ${PRJ}.ps
#	open ${PRJ}.ps &

view : ${PRJ}.pdf
	open ${PRJ}.pdf &

# change this to include what should be in the tarball,  use --exclude for exclusion.
pack : clean
	cd ..; /bin/rm -f ${PRJ}.tgz; tar -hzcf ${PRJ}.tgz ${PRJ}

clean:
	/bin/rm -f *.toc *.aux *.blg *.log *.dvi *~* *.bak *.out

distclean:
	/bin/rm -f *.toc *.aux *.blg *.log ${PRJ}.dvi ${PRJ}.ps ${PRJ}.pdf *~* *.bak *.out ${K2PDF_OUT}.pdf

# watch the options to dvips, which extract differnt pages from the dvi file.
cut : ${PRJ}.pdf
	pdfjam -o ${PRJ}-toc.pdf $< 1-2
	pdfjam -o ${PRJ}-summary.pdf $< 3
	pdfjam -o ${PRJ}-description.pdf $< 4-28
	pdfjam -o ${PRJ}-references.pdf $< 29-
